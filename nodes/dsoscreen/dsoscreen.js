const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function DSOScreen (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name
    }

    jared.addComponent(this)

    this.registerEvent('EVENT_SCREEN_UPDATED')
  }

  RED.nodes.registerType('DSOScreen', DSOScreen)
}
