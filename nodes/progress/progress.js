const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Progress (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      size: n.size,
      color: n.color,
      percent: Number(n.percent),
      label: n.label,
      active: n.active,
      disabled: n.disabled,
      hidden: n.hidden
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Progress', Progress)
}
