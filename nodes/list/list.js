const Jared = require('../..')

module.exports = function(RED) { // eslint-disable-line
  function List (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      style: n.style,
      action: n.action,
      actionPayload: n.actionPayload,
      rowWidth: Number(n.rowWidth),
      rowHeight: Number(n.rowHeight),
      rowPadding: n.rowPadding,
      showBorder: n.showBorder,
      divided: n.divided,
      selection: n.selection
    }

    jared.addComponent(this)

    this.registerEvent('LIST_ON_CLICK_ITEM')
  }

  RED.nodes.registerType('List', List)
}
