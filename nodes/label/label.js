const Jared = require('../..')

module.exports = function(RED) { // eslint-disable-line
  function Label (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      text: n.text,
      size: n.size,
      color: n.color,
      plaintext: n.plaintext,
      textAlign: n.textAlign,
      basic: n.basic,
      circular: n.circular
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Label', Label)
}
