const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Knob (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      min: Number(n.min),
      max: Number(n.max),
      step: Number(n.step),
      decimalDigits: Number(n.decimalDigits),
      throttle: Number(n.throttle),
      defaultValue: Number(n.defaultValue),
      displayValue: Boolean(n.displayValue),
      dataSet: n.dataSet.split(',').filter(v => v)
    }

    this.updatedProps = {}

    jared.addComponent(this)

    this.registerEvent('KNOB_ON_CHANGE')
  }

  RED.nodes.registerType('Knob', Knob)
}
