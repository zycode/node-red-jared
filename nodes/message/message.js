const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Message (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      title: n.title,
      body: n.body,
      size: n.size,
      style: n.style,
      color: n.color,
      textAlign: n.textAlign,
      icon: n.icon,
      loading: n.loading,
      hidden: n.hidden
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Message', Message)
}
