const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Dropdown (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      items: n.items,
      defaultValue: n.defaultValue,
      disabled: n.disabled
    }

    jared.addComponent(this)

    this.registerEvent('DROPDOWN_ON_CHANGE')
  }

  RED.nodes.registerType('Dropdown', Dropdown)
}
