# 按鈕 (Button)



## 屬性 (msg.props)

| 名稱 | 型態 | 格式 | 說明 |
|-----|------|-----|-----|
| name | String | 任意文字 | 按鈕的名稱。|
| text | String | 任意文字 | 按鈕上的文字。|
| size | String | `mini`<br>`tiny`<br>`small`<br>`medium`<br>`large`<br>`big`<br>`huge`<br>`massive` | 按鈕大小。|
| color | String | `red`<br>`orange`<br>`yellow`<br>`olive`<br>`green`<br>`teal`<br>`blue`<br>`violet`<br>`purple`<br>`pink`<br>`brown`<br>`grey`<br>`black`<br>`facebook`<br>`google plus`<br>`instagram`<br>`linkedin`<br>`twitter`<br>`vk`<br>`youtube` | 按鈕顏色。|
| icon | String | 參考 *Semantic UI* [圖示列表](https://react.semantic-ui.com/elements/icon/) | 按鈕上的小圖示。|
| basic | Boolean | true/false | 使用精簡的外觀。|
| circular | Boolean | true/false | 使用圖形的按鈕。|
| disabled | Boolean | true/false | 將按鈕設為禁用。|
| loadingOnClick | Boolean | true/false | 當按下按鈕時自動顯示載入動畫圖示。|
| action | String | `NodeSend`<br>`UIEvent`<br>`UINavigate`<br>`FileDownload`<br>`FileUpload` | 按鈕按下時的動作：<p><p>`NodeSend`: 在Node-RED中送出包含*actionPayload*的Click事件。<p>`UIEvent`: 在UI中送出包含*actionPayload*的Click事件。<p>`UINavigate`: 切換到*actionPayload*指定的面板或連結到指定的錨點。<p>`FileDownload`: 下載*actionPayload*指定的路徑的檔案。<p>`FileUpload`: 將File類型Input元件中指定的檔案資料傳送至Node-RED流程中處理。需搭配File類型的Input元件，其中*actionPayload*即設定為該Input元件的ID。
| actionPayload | String | 參考`action`屬性 | 此欄位的設定會因`action`屬性的選擇而不同。<p><p>`NodeSend`: 可為任意字串做為參數。<p>`UIEvent`: 可為任意字串為參數。<p>`UINavigate`:<p><table><tr><th><nobr>目標類型</nobr></th><th><nobr>格式範例</nobr></th><th>說明</th></tr><tr><td>單一面板</td><td>/demo</td><td>連結到 /jared/demo</td></tr><tr><td>列表面板</td><td>/demo/3</td><td>連結到 /jared/demo/3 (列表中的第3個demo面板)</td></tr><tr><td>單一錨點</td><td>#anchor</td><td>畫面移到 /jared/#anchor 錨點</td></tr><tr><td>列表錨點</td><td>#anchor@2</td><td>畫面移到 /jared/#anchor@2 (列表中第2個#anchor錨點)</td></tr><tr><td>返回前頁</td><td>Referer</td><td>返回跳轉到/login前的頁面。用於Login按鈕的設計，因為使用者登入成功後需要回到登入前瀏覽的頁面。</td></tr></table><p>`FileDownload`: 檔案的路徑。例如 /export/file.zip 的完整URL會轉換為 http://127.0.0.1:1880/jared/download/export/file.zip 。<p>`FileUpload`: File類型Input元件的ID。


## 資料輸入 (msg.payload)

N/A

## 命令及事件

| 代碼 | <nobr>類型</nobr> | <nobr>參數</nobr> | 回傳值 | 說明 |
|-----|------------------|-------------------|-------|-----|
| BUTTON_ON_CLICK | 事件 | actionPayload | N/A | 當使用者按下按鈕時會送出此命令。|

## 已知問題

N/A

## 範例代碼

```js
[
    {
        "id": "da398590.fca9e8",
        "type": "tab",
        "label": "Button",
        "disabled": false,
        "info": ""
    },
    {
        "id": "2fc38d8e.e37e62",
        "type": "Panel",
        "z": "da398590.fca9e8",
        "name": "Button Panel",
        "route": "/button",
        "backgroundColor": "",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 720,
        "y": 160,
        "wires": [
            []
        ]
    },
    {
        "id": "af2ab9b6.6f6538",
        "type": "Button",
        "z": "da398590.fca9e8",
        "name": "Demo Button",
        "text": "Demo",
        "size": "large",
        "color": "violet",
        "icon": "world",
        "basic": false,
        "circular": false,
        "disabled": false,
        "loadingOnClick": false,
        "action": "NodeSend",
        "actionPayload": "Clicked!",
        "x": 430,
        "y": 200,
        "wires": [
            [
                "2fc38d8e.e37e62",
                "8b0f9018.14ba8"
            ]
        ]
    },
    {
        "id": "8b0f9018.14ba8",
        "type": "debug",
        "z": "da398590.fca9e8",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "true",
        "x": 670,
        "y": 260,
        "wires": []
    }
]
```
