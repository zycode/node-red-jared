const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function LineChart (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      backgroundColor: n.backgroundColor,
      xAxisDataKey: n.xAxisDataKey,
      xAxisUnit: n.xAxisUnit,
      yAxisUnit: n.yAxisUnit,
      showXAxis: Boolean(n.showXAxis),
      showYAxis: Boolean(n.showYAxis),
      showAxisLabel: Boolean(n.showAxisLabel),
      showCartesianGrid: Boolean(n.showCartesianGrid),
      showTooltip: Boolean(n.showTooltip),
      showLegend: Boolean(n.showLegend)
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('LineChart', LineChart)
}
