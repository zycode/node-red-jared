# UI事件來源 (EventSource)



## 屬性 (msg.props)

| 名稱 | 型態 | 格式 | 說明 |
|-----|------|-----|-----|
| name | String | 任意文字 | 事件來源的名稱。|

## 資料輸入 (msg.payload)

N/A

## 命令及事件

| 名稱 | 類型 | 對象 | 格式 | 說明 |
|-----|------|-----|-----|-----|
| PANEL_ON_LOAD | 事件 | Panel | TODO | 當路由面板載入時觸發此事件。|
| PANEL_ON_UNLOAD | 事件 | Panel | TODO | 當離開路由面板時觸發此事件。|
| LIST_ON_UPDATE_ROUTE_INDEX | 事件 | List | TODO | TODO |
| USER_LOGGED_IN | 事件 | 系統 | TODO | 當使用者登入後觸發此事件。|
| USER_LOGGED_OUT | 事件 | 系統 | TODO | 當使用者登出後觸發此事件。|
| USER_LOGIN_FAILED | 事件 | 系統 | TODO | 當使用者登入失敗時觸發此事件。|
| * | 事件 | 全部 | TODO | 任何事件發生時皆觸發。|

## 已知問題

N/A

## 範例代碼

```js
```
