const Jared = require('../..')

module.exports = function(RED) { // eslint-disable-line
  function EventSource (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      event: n.event,
      source: n.source
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('EventSource', EventSource)
}
