const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function BarChart (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      backgroundColor: n.backgroundColor,
      xAxisDataKey: n.xAxisDataKey,
      showXAxis: Boolean(n.showXAxis),
      showYAxis: Boolean(n.showYAxis),
      showCartesianGrid: Boolean(n.showCartesianGrid),
      showTooltip: Boolean(n.showTooltip),
      showLegend: Boolean(n.showLegend)
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('BarChart', BarChart)
}
