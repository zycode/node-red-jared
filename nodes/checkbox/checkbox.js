const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Checkbox (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      checked: n.checked,
      disabled: n.disabled
    }

    jared.addComponent(this)

    this.registerEvent('CHECKBOX_ON_CHANGE')
  }

  RED.nodes.registerType('Checkbox', Checkbox)
}
