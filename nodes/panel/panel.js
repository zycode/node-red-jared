const Jared = require('../..')

module.exports = function(RED) { // eslint-disable-line
  function Panel (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      route: n.route ? n.route.replace(/ /g, '').split(',') : '',
      backgroundColor: n.backgroundColor,
      zIndex: Number(n.zIndex),
      portal: n.portal,
      showBorder: n.showBorder
    }

    jared.addComponent(this, {
      nodeEventHandlers: {
        /* Use default event handler */
        PANEL_ON_LOAD: null,
        PANEL_ON_UNLOAD: null
      },
      nodeInputHandler: (msg) => {
        if (msg.payload !== undefined) {
          this.rtc.broadcast(this, {
            dataType: 'data',
            payload: msg.payload,
            index: msg.index
          })
        }

        if (msg.props) {
          this.props = { ...this.props, ...msg.props }
          if (msg.props.route) {
            this.props.route = msg.props.route.split(',')
          }
          this.rtc.broadcast(this, {
            dataType: 'props',
            payload: msg.props,
            index: msg.index
          })
        }

        if (this.props.route && msg.menu) {
          this.rtc.broadcast(this, {
            dataType: 'menu',
            payload: msg.menu
          })
        }
      }
    })
  }

  RED.nodes.registerType('Panel', Panel)
}
