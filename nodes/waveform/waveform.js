const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Waveform (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      backgroundColor: n.backgroundColor,
      xAxisMaxDisplayLength: Number(n.xAxisMaxDisplayLength),
      showAxisLabel: Boolean(n.showAxisLabel)
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Waveform', Waveform)
}
