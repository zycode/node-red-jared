const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Header (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      text: n.text,
      size: n.size,
      color: n.color,
      textAlign: n.textAlign,
      icon: n.icon,
      dividing: n.dividing,
      disabled: n.disabled
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Header', Header)
}
