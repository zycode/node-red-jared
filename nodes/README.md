# Jared UI Nodes

## Default Property

## Update Properties

```js
// In a function node
node.send({
  props: {
    ...componentProps
  }
})
```

## Send Input Data

```js
// In a function node
node.send({
  payload: {
    ...componentData
  }
})
```

### Input Data Format

#### Single data for single node

```js
{
  payload: {
    ...data
  }
}
```

#### Array data for single node

```js
{
  payload: [
    ...data
  ]
}
```

#### Single data for node inside a list

```js
{
  payload: {
    listData: [
      { ...data0 }, // Object data for list item at index 0
      { ...data1 }, // Object data for list item at index 1
      ...
    ]
  }
}
```

#### Array data for node inside a list

```js
{
  payload: {
    listData: [
      [ ...data0 ], // Array data for list item at index 0
      [ ...data1 ], // Array data for list item at index 1
      ...
    ]
  }
}
```

#### Single data for node at specified index inside a list

```js
{
  index: 1, // Data for list item at index 1
  payload: {
    ...data
  }
}
```

#### Array data for node at specified index inside a list

```js
{
  index: 1, // Data for list item at index 1
  payload: [
    ...data
  ]
}
```
