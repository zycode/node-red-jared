const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Notification (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      title: n.title,
      message: n.message,
      level: n.level,
      autoDismiss: Number(n.autoDismiss),
      dismissable: n.dismissable
    }

    jared.addComponent(this, {
      nodeInputHandler: (msg) => {
        const { notification, payload } = msg
        this.rtc.broadcast(this, {
          dataType: 'notification',
          payload: {
            uid: this.props.id,
            method: payload === 'Dismiss' ? 'Dismiss' : 'Add',
            ...(notification || this.props)
          }
        })
      }
    })
  }

  RED.nodes.registerType('Notification', Notification)
}
