const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Input (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      defaultValue: n.defaultValue,
      placeholder: n.placeholder,
      size: n.size,
      textAlign: n.textAlign,
      inputType: n.inputType,
      min: Number(n.min),
      max: Number(n.max),
      step: Number(n.step),
      icon: n.icon,
      iconPosition: n.iconPosition,
      disabled: n.disabled
    }

    jared.addComponent(this)

    this.registerEvent('INPUT_ON_CHANGE')
  }

  RED.nodes.registerType('Input', Input)
}
