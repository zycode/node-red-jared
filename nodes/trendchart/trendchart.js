const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function TrendChart (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.trendData = {}
    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      backgroundColor: n.backgroundColor,
      xAxisMaxDisplayLength: Number(n.xAxisMaxDisplayLength),
      xAxisTimeTickFormat: n.xAxisTimeTickFormat,
      yAxisName: n.yAxisName,
      yAxisUnit: n.yAxisUnit,
      useMetricPrefix: Boolean(n.useMetricPrefix),
      showAxisLabel: Boolean(n.showAxisLabel)
    }

    jared.addComponent(this, {
      nodeInputHandler: (msg) => {
        // console.log(require('util').inspect(msg))
        if (msg.payload !== undefined) {
          if (Array.isArray(msg.payload.listData)) {
            msg.payload.listData.forEach((data, i) => { this.trendData[i] = data })
          } else {
            const index = isNaN(msg.index) ? 0 : msg.index
            this.trendData[index] = this.trendData[index] || []

            if (Array.isArray(msg.payload)) {
              this.trendData[index] = msg.payload
            } else {
              this.trendData[index].push(msg.payload)
            }
          }
          // console.log('send data', require('util').inspect(msg.payload))
          this.rtc.broadcast(this, {
            dataType: 'data',
            payload: msg.payload,
            index: msg.index
          })
        }

        if (msg.props) {
          this.props = { ...this.props, ...msg.props }
          this.rtc.broadcast(this, {
            dataType: 'props',
            payload: msg.props,
            index: msg.index
          })
        }
      }
    })

    this.registerEvent('CHART_DO_SYNC', (event, done) => {
      const arrayData = this.trendData[isNaN(event.index) ? 0 : event.index]
      if (arrayData) {
        this.rtc.broadcast(this, {
          dataType: 'data',
          payload: arrayData,
          index: event.index
        })
      }
      done && done()
    })
  }

  RED.nodes.registerType('TrendChart', TrendChart)
}
