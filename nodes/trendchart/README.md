# 趨勢圖 (Trend Chart)

一種在時間序列中顯示數據變化及走勢的圖表。

## 屬性 (msg.props)

| 名稱 | 型態 | 格式 | 說明 |
|-----|------|-----|-----|
| name | String | 任意文字 | 圖示的名稱。|
| xAxisMaxDisplayLength | Number | 整數 | 圖表上最多同時顯示的資料數量。若累積的資料量超過此數值，則圖表會擷取最新的資料區間來顯示。當然，你可以在圖表上向右拖曳來檢視較先前的資料。|
| xAxisTimeTickFormat | String | <nobr>`mm:ss`<br>`hh:mm`<br>`hh:mm:ss`<br>`DD`<br>`MM`<br>`YYYY`<br>`MM/DD`<br>`YYYY/MM`<br>`YYYY/MM/DD`<br>`YYYY/MM/DD hh:mm`<br>`YYYY/MM/DD hh:mm:ss`</nobr> | 時間軸(X軸)顯示時間的格式，其中：<br>`Y`: Year<br>`M`: Month<br>`D`: Day<br>`h`: Hour<br>`m`: Minute<br>`s`: Second |
| yAxisName | String | 任意文字 | 資料軸(Y軸)的名稱。|
| yAxisUnit | String | 任意文字 | 資料的單位。|
| useMetricPrefix | Boolean | true/false | 使用國際單位制前綴詞，如：`1000Hz`會轉換為`1KHz`；`0.001A`會轉換為`1mA`。|
| showAxisLabel | Boolean | true/false | 是否顯示坐標軸的文字標籤。|

## 資料輸入 (msg.payload)

| 型態 | 格式 | 說明 |
|-----|------|-----|
| Object | <nobr>payload: {<br>&nbsp;&nbsp;&nbsp;&nbsp;`timestamp`: _**{Number}**_,<br>&nbsp;&nbsp;&nbsp;&nbsp;`value`: _**{Number}**_<br>}</nobr> | 插入單筆資料，資料會在圖表中自動累積。<br>`timestamp`: 資料產生時的時間戳記。<br>`value`: 資料的數值。|
| Array | <nobr>payload: [{<br>&nbsp;&nbsp;&nbsp;&nbsp;`timestamp`: _**{Number}**_,<br>&nbsp;&nbsp;&nbsp;&nbsp;`value`: _**{Number}**_<br>}, ...]</nobr> | 設定初始資料集，可用來預先載入舊有的資料。資料格式說明同下。|

## 命令及事件

| 代碼 | <nobr>類型</nobr> | <nobr>參數</nobr> | 回傳值 | 說明 |
|-----|------|-------------------|-------|-----|
| CHART_DO_SYNC | 命令 | N/A | 回傳完整的資料集(Array)給趨勢圖UI元件做資料同步。 | 通常在趨勢圖UI元件剛被顯示在畫面上時會呼叫一次此命令。 |

## 已知問題

- 在List中，若Panel載入時沒有為圖表輸入初始資料集，圖表會因為沒有被顯示(mount)而無法發出同步命令來擷取server已存在的資料。

## 範例代碼

```js
[
    {
        "id": "a1b721c1.03c78",
        "type": "tab",
        "label": "Flow 1",
        "disabled": false,
        "info": ""
    },
    {
        "id": "59792e0c.c4e7c",
        "type": "TrendChart",
        "z": "a1b721c1.03c78",
        "name": "RPM",
        "xAxisMaxDisplayLength": 100,
        "xAxisTimeTickFormat": "mm:ss",
        "yAxisName": "Speed",
        "yAxisUnit": "RPM",
        "useMetricPrefix": false,
        "showAxisLabel": true,
        "x": 570,
        "y": 340,
        "wires": [
            [
                "ab67e6d2.9c6688"
            ]
        ]
    },
    {
        "id": "93e0f69f.a90eb8",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "",
        "topic": "Add data #0",
        "payload": "0",
        "payloadType": "num",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 150,
        "y": 280,
        "wires": [
            [
                "bbf65f6e.a2fae"
            ]
        ]
    },
    {
        "id": "4f8e8a26.512c44",
        "type": "function",
        "z": "a1b721c1.03c78",
        "name": "toDataSource",
        "func": "// return {\n//     payload: [{\n//         timestamp: msg.payload,\n//         value: Math.floor(Math.random() * 10000) + 1000\n//     }, {\n//         timestamp: msg.payload,\n//         value: Math.floor(Math.random() * 8000) + 6000\n//     }]\n// }\nreturn {\n    payload: [\n        Array(10).fill(0).map(_ => ({ timestamp: Date.now(), value: Math.floor(Math.random() * 1000) })),\n        Array(10).fill(0).map(_ => ({ timestamp: Date.now(), value: Math.floor(Math.random() * 1000) }))\n    ]\n}\n",
        "outputs": 1,
        "noerr": 0,
        "x": 360,
        "y": 240,
        "wires": [
            [
                "59792e0c.c4e7c"
            ]
        ]
    },
    {
        "id": "946799bd.8182f8",
        "type": "List",
        "z": "a1b721c1.03c78",
        "name": "Chart List",
        "style": "list",
        "action": "server command",
        "payload": "Chardi",
        "rowWidth": 256,
        "rowHeight": "250",
        "rowPadding": "",
        "showBorder": true,
        "divided": true,
        "selection": false,
        "x": 620,
        "y": 180,
        "wires": [
            [
                "55bdfa21.238114"
            ]
        ]
    },
    {
        "id": "ab67e6d2.9c6688",
        "type": "Panel",
        "z": "a1b721c1.03c78",
        "name": "Chart List Item",
        "route": "",
        "backgroundColor": "",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 600,
        "y": 260,
        "wires": [
            [
                "946799bd.8182f8"
            ]
        ]
    },
    {
        "id": "bbf65f6e.a2fae",
        "type": "function",
        "z": "a1b721c1.03c78",
        "name": "Add data",
        "func": "return {\n    index: Number(msg.payload),\n    payload: {\n        timestamp: Date.now(),\n        value: Math.floor(Math.random() * 1000) + 1000\n    }\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 380,
        "y": 300,
        "wires": [
            [
                "59792e0c.c4e7c"
            ]
        ]
    },
    {
        "id": "7416c130.277ee",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "",
        "topic": "Add data #1",
        "payload": "1",
        "payloadType": "num",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 150,
        "y": 320,
        "wires": [
            [
                "bbf65f6e.a2fae"
            ]
        ]
    },
    {
        "id": "855e8066.c9248",
        "type": "TrendChart",
        "z": "a1b721c1.03c78",
        "name": "Voltage",
        "xAxisMaxDisplayLength": 100,
        "xAxisTimeTickFormat": "mm:ss",
        "yAxisName": "Voltage",
        "yAxisUnit": "V",
        "useMetricPrefix": true,
        "showAxisLabel": true,
        "x": 620,
        "y": 100,
        "wires": [
            [
                "55bdfa21.238114"
            ]
        ]
    },
    {
        "id": "f228d440.d4aa38",
        "type": "function",
        "z": "a1b721c1.03c78",
        "name": "Init V",
        "func": "return {\n    payload: Array(10).fill(0).map(_ => ({ timestamp: Date.now(), value: Math.random() * 0.1 }))\n    }",
        "outputs": 1,
        "noerr": 0,
        "x": 450,
        "y": 80,
        "wires": [
            [
                "855e8066.c9248"
            ]
        ]
    },
    {
        "id": "67e4e406.c361bc",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "Init",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 250,
        "y": 80,
        "wires": [
            [
                "f228d440.d4aa38"
            ]
        ]
    },
    {
        "id": "691bf3f8.9413bc",
        "type": "function",
        "z": "a1b721c1.03c78",
        "name": "Set V",
        "func": "return {\n    payload: { \n        timestamp: Date.now(), \n        value: Math.random() * 0.1\n    }\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 450,
        "y": 120,
        "wires": [
            [
                "855e8066.c9248"
            ]
        ]
    },
    {
        "id": "b425ff0.af931",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "Add",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 250,
        "y": 120,
        "wires": [
            [
                "691bf3f8.9413bc"
            ]
        ]
    },
    {
        "id": "69ca3b69.bda654",
        "type": "function",
        "z": "a1b721c1.03c78",
        "name": "Update item props",
        "func": "return {\n    index: Number(msg.payload),\n    props: {\n        xAxisTimeTickFormat: 'hh:mm:ss'\n    }\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 350,
        "y": 380,
        "wires": [
            [
                "59792e0c.c4e7c"
            ]
        ]
    },
    {
        "id": "4efe7421.e4d45c",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "",
        "topic": "Update props #0",
        "payload": "0",
        "payloadType": "num",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 130,
        "y": 360,
        "wires": [
            [
                "69ca3b69.bda654"
            ]
        ]
    },
    {
        "id": "118246ab.4cd9a9",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "",
        "topic": "Update props #1",
        "payload": "1",
        "payloadType": "num",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 130,
        "y": 400,
        "wires": [
            [
                "69ca3b69.bda654"
            ]
        ]
    },
    {
        "id": "358114e3.56effc",
        "type": "function",
        "z": "a1b721c1.03c78",
        "name": "Update all props",
        "func": "return {\n    props: [\n        { xAxisTimeTickFormat: 'hh:mm' },\n        { xAxisTimeTickFormat: 'hh:mm' }\n    ]\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 350,
        "y": 440,
        "wires": [
            [
                "59792e0c.c4e7c"
            ]
        ]
    },
    {
        "id": "d8a8059d.00aef8",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "Update all",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 160,
        "y": 440,
        "wires": [
            [
                "358114e3.56effc"
            ]
        ]
    },
    {
        "id": "511a1297.2aff5c",
        "type": "inject",
        "z": "a1b721c1.03c78",
        "name": "Init all",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 170,
        "y": 240,
        "wires": [
            [
                "4f8e8a26.512c44"
            ]
        ]
    },
    {
        "id": "55bdfa21.238114",
        "type": "Panel",
        "z": "a1b721c1.03c78",
        "name": "Chart Panel",
        "route": "/chart",
        "backgroundColor": "",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 830,
        "y": 140,
        "wires": [
            []
        ]
    }
]
```
