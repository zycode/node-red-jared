const path = require('path')
const AppInfoManager = require('./info')
const UserManager = require('./user')
const PanelLayoutFileManager = require('./layout')
const LocaleFileManager = require('./locale')
const ComponentManager = require('./component')
const RTConnectionManager = require('./rtc')
const { MODULE_NAME } = require('../constant')

function createManagers (RED) {
  const appInfoManager = new AppInfoManager(RED.settings.userDir)
  const userManager = new UserManager(`http://localhost:${RED.settings.uiPort}${RED.settings.httpAdminRoot}`)
  const panelLayoutFileManager = new PanelLayoutFileManager(path.join(RED.settings.userDir, MODULE_NAME, 'layouts'))
  const localeFileManager = new LocaleFileManager(path.join(RED.settings.userDir, MODULE_NAME, 'locales'))
  const componentManager = new ComponentManager()
  const connectionManager = new RTConnectionManager(RED.server, { path: `${RED.settings.jaredRoot}/io` })

  return {
    appInfoManager,
    userManager,
    panelLayoutFileManager,
    localeFileManager,
    componentManager,
    connectionManager
  }
}

module.exports = {
  AppInfoManager,
  UserManager,
  LocaleFileManager,
  ComponentManager,
  PanelLayoutFileManager,
  RTConnectionManager,
  createManagers
}
