const { JARED_SYSTEM_COMPONENT_ID } = require('../constant')

class ComponentManager {
  constructor () {
    this.componentNodes = []
    this.panelNodes = []
    this.eventSourceNodes = []
    this.applyJCID = this.applyJCID.bind(this)
    this.handleInputMessage = this.handleInputMessage.bind(this)
  }

  /* Return pure object type this.componentNodes */
  get components () {
    return this.componentNodes.map(this.applyJCID)
  }

  /* Return pure object type panels */
  get panels () {
    return this.panelNodes.map(this.applyJCID)
  }

  /* Return pure object type event sources */
  get eventSources () {
    return this.eventSourceNodes.map(this.applyJCID)
  }

  setGlobalContext (name, value) {
    this.componentNodes[0].context().global.set(name, value)
  }

  applyJCID (node) {
    return {
      ...node.props,
      id: node.props.jcid,
      wires: node.wires.map(wire => wire.map((id) => {
        const next = this.componentNodes.find(c => c.id === id)
        return next ? next.props.jcid : id
      }))
    }
  }

  getDefaultHandler (type, node) {
    const handlers = {
      input: (msg) => {
        if (msg.payload !== undefined) {
          node.rtc.broadcast(node, {
            dataType: 'data',
            payload: msg.payload,
            index: msg.index
          })
        }

        if (msg.props) {
          node.props = {
            ...node.props,
            ...msg.props,
            // set below to prevent id and jcid from being overrided by msg
            id: node.props.id,
            jcid: node.props.jcid
          }
          node.rtc.broadcast(node, {
            dataType: 'props',
            payload: msg.props,
            index: msg.index
          })
        }
      },
      event: (event) => {
        // console.log(this.props.type, require('util').inspect(event))
        node.send({ component: node.props, ...event })
      },
      close: (removed, done) => {
        this.removeComponent(node)
        done()
      }
    }

    return handlers[type]
  }

  prepareComponent (node, options) {
    node.defaultProps = { ...node.props }
    node.eventHandlers = {}
    node.props.wires = node.wires

    node.registerEvent = (event, handler) => {
      if (handler) {
        handler = handler.bind(node)
      }
      node.eventHandlers[event] = handler || this.getDefaultHandler('event', node)
    }

    /* Install custom component node event handlers */
    if (options.nodeEventHandlers) {
      Object.keys(options.nodeEventHandlers).forEach((eventName) => {
        node.registerEvent(eventName, options.nodeEventHandlers[eventName])
      })
    }

    if (options.nodeInputHandler) {
      options.nodeInputHandler = options.nodeInputHandler.bind(node)
    }

    if (options.nodeCloseHandler) {
      options.nodeCloseHandler = options.nodeCloseHandler.bind(node)
    }

    node.on('input', options.nodeInputHandler || this.getDefaultHandler('input', node))
    node.on('close', options.nodeCloseHandler || this.getDefaultHandler('close', node))

    if (node.props.type === 'EventSource') {
      node.on('event', this.getDefaultHandler('event', node))
    } else {
      node.on('event', (event) => {
        const handler = node.eventHandlers[event.type]
        if (handler) {
          handler(event)
        } else {
          console.log(`Event ${event.type} fired but component ${node.props.name} has no handler for it.`)
        }
      })
    }
  }

  clearComponent (node) {
    /* Stop data propagation after component node removed */
    node.send = function () {}
    node.removeAllListeners('input')
    node.removeAllListeners('close')
    node.removeAllListeners('event')
  }

  addComponent (node) {
    if (node.props.type === 'EventSource') {
      this.addEventSourceNode(node)
    } else if (node.props.type === 'Notification') {
      this.addNotificationNode(node)
    } else {
      this.addUINode(node)
    }
  }

  addUINode (node) {
    const idx = this.componentNodes.findIndex(c => c.props.jcid === node.props.jcid)
    if (idx < 0) {
      this.componentNodes.push(node)

      if (['Panel', 'List'].includes(node.props.type)) {
        this.addPanelNodes(node)
      }
      console.log(`Jared component added (jcid = ${node.props.jcid}, type = ${node.props.type})`)
    } else {
      this.componentNodes[idx] = node
      console.log(`Jared component updated (jcid = ${node.props.jcid}, type = ${node.props.type})`)
    }
  }

  addPanelNodes (node) {
    if (node.props.main) {
      // replace main panel
      this.panelNodes.shift()
      this.panelNodes.unshift(node)
    } else {
      this.panelNodes.push(node)
    }
  }

  addEventSourceNode (node) {
    const idx = this.eventSourceNodes.findIndex(c => c.props.jcid === node.props.jcid)
    if (idx < 0) {
      this.eventSourceNodes.push(node)
    } else {
      this.eventSourceNodes[idx] = node
    }
  }

  addNotificationNode (node) {
    // Do nothing because Notification has nothing to do with panel, it works in its own way
  }

  removeComponent (node) {
    this.clearComponent(node)

    if (node.props.type === 'EventSource') {
      this.removeEventSourceNode(node)
    } else {
      this.removeUINode(node)
    }
  }

  removeUINode (node) {
    if (removeNodeFromArray(this.componentNodes, node) >= 0) {
      if (['Panel', 'List'].includes(node.props.type)) {
        removeNodeFromArray(this.panelNodes, node)
      }
      console.log(`Jared component removed (id = ${node.props.jcid}, type = ${node.props.type})`)
    }
  }

  removeEventSourceNode (node) {
    removeNodeFromArray(this.eventSources, node)
  }

  handleInputMessage (msg, cb) {
    const { id, index, payload } = msg
    const node = this.componentNodes.find(c => c.props.jcid === id)
    const evdata = {
      type: payload.command,
      payload: payload.parameters
    }

    if (!isNaN(index)) {
      evdata.index = Number(index)
    }

    if (id === JARED_SYSTEM_COMPONENT_ID || node) {
      this.eventSourceNodes.forEach((esnode) => {
        if (isEventSourceMatchTargetNode(esnode, node || id) &&
          shouldEventSourceTriggerEvent(esnode, payload.command)) {
          esnode.emit('event', evdata)
        }
      })
    }

    if (node) {
      node.emit('event', evdata)
      cb()
    }
  }
}

module.exports = ComponentManager

function removeNodeFromArray (array, node) {
  const idx = array.findIndex(c => c.props.jcid === node.props.jcid)
  if (idx >= 0) {
    array.splice(idx, 1)
  }
  return idx
}

function isEventSourceMatchTargetNode (eventSourceNode, targetNode) {
  return (
    targetNode === JARED_SYSTEM_COMPONENT_ID ||
    eventSourceNode.props.source.includes('*') ||
    eventSourceNode.props.source.includes(targetNode.props.jcid) ||
    eventSourceNode.props.source.includes(targetNode.props.name)
  )
}

function shouldEventSourceTriggerEvent (eventSourceNode, eventType) {
  return (
    eventSourceNode.props.event.includes('*') ||
    eventSourceNode.props.event === eventType
  )
}
