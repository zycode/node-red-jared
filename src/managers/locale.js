const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')

class LocaleFileManager {
  constructor (localeDir) {
    this.basePath = localeDir
    mkdirp.sync(this.basePath)
  }

  read (lng, ns) {
    return new Promise((resolve, reject) => {
      const file = path.join(this.basePath, `${lng}/${ns}.json`)
      fs.readFile(file, (readError, data) => {
        if (!readError) {
          try {
            resolve(JSON.parse(data.toString()))
          } catch (parseError) {
            console.error(`Unable to parse locale file: ${file}, error: ${parseError.message}`)
            reject(parseError)
          }
        } else {
          if (readError.code === 'ENOENT') resolve({})
          else reject(readError)
        }
      })
    })
  }

  write (lng, ns, data) {
    return new Promise((resolve, reject) => {
      const file = path.join(this.basePath, `${lng}/${ns}.json`)
      mkdirp.sync(path.dirname(file))
      this.read(lng, ns)
        .then((trans) => {
          const translation = { ...trans, ...data }
          fs.writeFile(file, JSON.stringify(translation, null, 2), (error) => {
            if (error) reject(error)
            else resolve()
          })
        })
        .catch(reject)
    })
  }
}

module.exports = LocaleFileManager
