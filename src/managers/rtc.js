const util = require('util')
const socketio = require('socket.io')

class RTConnectionManager {
  constructor (httpServer, options) {
    this.sockets = []
    this.server = socketio(httpServer, { path: options.path })
    this.messageHandler = (msg, cb) => { cb() }
    this.broadcast = this.broadcast.bind(this)
  }

  start () {
    this.server.on('connection', (socket) => {
      console.log(`A client connected to socket io with socket id = ${socket.id}`)
      this.sockets.push(socket)

      socket.on('disconnect', (reason) => {
        let idx = this.sockets.findIndex(sock => sock.id === socket.id)
        if (idx >= 0) {
          this.sockets.splice(idx, 1)
          console.log(`A client disconnected with socket id = ${socket.id}`)
        }
      })

      socket.on('message', (msg, cb) => {
        if (!msg || msg.target === undefined) {
          console.log('Unable to handle input message', util.inspect(msg))
          return cb()
        }
        const [id, index] = msg.target.split('@')
        this.messageHandler({ id, index, payload: msg.payload }, cb)
      })
    })
  }

  broadcast (node, msg) {
    this.sockets.forEach(function (socket) {
      socket.emit(msg.dataType, {
        target: isNaN(msg.index) ? node.props.jcid : `${node.props.jcid}@${msg.index}`,
        payload: msg.dataType === 'data' ? msg.payload : undefined,
        props: msg.dataType === 'props' ? msg.payload : undefined,
        menu: msg.dataType === 'menu' ? msg.payload : undefined,
        notification: msg.dataType === 'notification' ? msg.payload : undefined
      })
    })
  }

  attach (node) {
    node.rtc = { broadcast: this.broadcast }
  }

  detach (node) {
    delete node.rtc
  }

  onReceiveMessage (handler) {
    this.messageHandler = handler
  }
}

module.exports = RTConnectionManager
