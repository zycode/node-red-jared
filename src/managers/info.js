const fs = require('fs')
const path = require('path')

class AppInfoManager {
  constructor (appDir) {
    const packageJson = fs.readFileSync(path.join(appDir, 'package.json'))
    this.package = JSON.parse(packageJson)
  }

  get name () {
    return this.package.name
  }

  get version () {
    return this.package.version
  }

  get mode () {
    return process.env.NODE_ENV
  }

  toObject () {
    return {
      name: this.name,
      version: this.version,
      mode: this.mode
    }
  }
}

module.exports = AppInfoManager
