const fs = require('fs')
const path = require('path')
const serveStatic = require('serve-static')
const bodyParser = require('body-parser')
const cors = require('cors')
const mkdirp = require('mkdirp')
const dotenv = require('dotenv')

const { createManagers } = require('./managers')
const { createControllers } = require('./controllers')
const { createRouter } = require('./router')
const { MODULE_NAME, JARED_UI_ROOT } = require('./constant')

const privates = new WeakMap()
let instance = null

dotenv.config({ path: path.resolve(process.cwd(), `${MODULE_NAME}.env`) })

class JaredServer {
  constructor (RED) {
    if (instance) {
      return instance
    }

    instance = this

    // TODO: load user authentication strategy from RED.settings

    // set default jared root if necessary
    if (!RED.settings.jaredRoot) {
      RED.settings.jaredRoot = JARED_UI_ROOT
    }

    const managers = createManagers(RED)
    const controllers = createControllers(managers)
    const router = createRouter(controllers)

    privates.set(this, managers)

    this.start(RED, router)

    RED.log.info(`Jared server started at ${path.join(RED.settings.httpNodeRoot, RED.settings.jaredRoot)}`)
  }

  start (RED, router) {
    const { componentManager, connectionManager } = privates.get(this)

    const app = RED.httpNode || RED.httpAdmin
    const baseDataPath = path.join(RED.settings.userDir, MODULE_NAME)
    const assetPath = path.join(baseDataPath, 'assets')
    const uploadPath = path.join(baseDataPath, 'data/upload')
    const downloadPath = path.join(baseDataPath, 'data/download')
    let jaredModulePath = path.join(RED.settings.userDir, 'node_modules/jared')
    let jaredIndexHtmlPath = path.join(jaredModulePath, 'index.html')
    try {
      // test if we are running with development module
      if (fs.statSync(path.join(jaredModulePath, 'build'))) {
        jaredModulePath = path.join(jaredModulePath, 'build')
        jaredIndexHtmlPath = path.join(jaredModulePath, 'index.html')
      }
    } catch (e) {}

    // create jared paths
    mkdirp.sync(assetPath)
    mkdirp.sync(uploadPath)
    mkdirp.sync(downloadPath)

    app.use(cors())
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(RED.settings.jaredRoot, serveStatic(jaredModulePath))
    app.use(`${RED.settings.jaredRoot}/public`, serveStatic(assetPath))
    app.use(`${RED.settings.jaredRoot}/download`, serveStatic(downloadPath))
    app.use(`${RED.settings.jaredRoot}/api`, router)
    // handle /ui/* properly to avoid 404 not found
    app.get(`${RED.settings.jaredRoot}/*`, (req, res) => {
      res.sendFile(jaredIndexHtmlPath)
    })

    connectionManager.onReceiveMessage(componentManager.handleInputMessage)
    connectionManager.start()
  }

  /**
   * Add UI component to Jared core
   * @param {Object} node UI component node
   * @param {Object} options
   *   {
   *     nodeInputHandler: {Function} Custom node data input handler,
   *     nodeCloseHandler: {Function} Custom node close handler,
   *     nodeEventHandlers: {Object} Custom node event handlers object
   *       {
   *         Event1: {Function} Event 1 handler,
   *         Event2: {Function} Event 2 handler,
   *         ...
   *       }
   *   }
   */
  addComponent (node, options = {}) {
    const { appInfoManager, componentManager, connectionManager } = privates.get(this)

    componentManager.prepareComponent(node, options)
    componentManager.addComponent(node)
    /* Setup global context variables for Node-RED nodes to access AppInfo through `this.context().global.AppInfo` */
    componentManager.setGlobalContext('AppInfo', appInfoManager.toObject())
    connectionManager.attach(node)
  }

  removeComponent (node) {
    const { componentManager, connectionManager } = privates.get(this)
    componentManager.removeComponent(node)
    connectionManager.detach(node)
  }
}

module.exports = function (RED) {
  return new JaredServer(RED)
}
