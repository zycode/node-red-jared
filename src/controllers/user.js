module.exports = function (manager) {
  return {
    getCurrentUser (req, res) {
      manager.getCurrentUser({ reqHeaders: req.headers })
        .then(settings => res.json(settings.user))
        .catch((error) => {
          if (error.status === 401) res.json({})
          else res.sendStatus(error.status)
        })
    },

    signup (req, res) {
      // TBD
    },

    login (req, res) {
      manager.login({
        username: req.body.username,
        password: req.body.password
      })
        .then(token => res.json(token))
        .catch(error => res.sendStatus(error.status))
    },

    logout (req, res) {
      manager.logout({
        reqHeaders: req.headers,
        body: JSON.stringify({
          token: req.body.token
        })
      })
        .then(response => res.status(response.status).send(response.statusText))
        .catch(error => res.sendStatus(error.status))
    }
  }
}
